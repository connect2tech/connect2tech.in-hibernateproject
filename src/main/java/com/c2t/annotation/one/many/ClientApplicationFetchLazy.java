package com.c2t.annotation.one.many;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;

import com.c2t.xml.one.one.StockDetail;

public class ClientApplicationFetchLazy {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Stock> stocks = session.createQuery("from Stock").list();

		System.out.println("---------------1--------------" + stocks.size());

		for (int i = 0; i < stocks.size(); i++) {
			Iterator iter = stocks.get(i).getStockDailyRecords().iterator();
			while(iter.hasNext()){
				StockDailyRecord sd = (StockDailyRecord)iter.next();
				System.out.println(sd.getDate());
				System.out.println("--------------2---------------");
			}
			
			
		}

	}
}
