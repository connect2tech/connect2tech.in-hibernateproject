package com.c2t.annotation.one.many;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;


public class ClientApplication {
	public static void main(String[] args) {
		
		Set<StockDailyRecord> dailyRecords = new HashSet<StockDailyRecord>(
				0);
		
		System.out.println("Hibernate one to many (Annotation)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		

		Stock stock = new Stock();
        stock.setStockCode("7054");
        stock.setStockName("PADINI44");

        
        StockDailyRecord stockDailyRecords = new StockDailyRecord();
        stockDailyRecords.setPriceOpen(new Float("1.2"));
        stockDailyRecords.setPriceClose(new Float("1.1"));
        stockDailyRecords.setPriceChange(new Float("10.0"));
        stockDailyRecords.setVolume(3000000L);
        stockDailyRecords.setDate(new Date());
        
        stockDailyRecords.setStock(stock);
        
        dailyRecords.add(stockDailyRecords);
        

        
        StockDailyRecord stockDailyRecords2 = new StockDailyRecord();
        stockDailyRecords2.setPriceOpen(new Float("1.3"));
        stockDailyRecords2.setPriceClose(new Float("1.3"));
        stockDailyRecords2.setPriceChange(new Float("10.3"));
        stockDailyRecords2.setVolume(3000000L);
        Date someDate = new Date();
        someDate.setDate(someDate.getDate() + 1); 
        stockDailyRecords2.setDate(someDate);
        
        stockDailyRecords2.setStock(stock);
        dailyRecords.add(stockDailyRecords2);
        
        stock.setStockDailyRecords(dailyRecords);
        
        
        session.beginTransaction();

        session.save(stock);
        session.save(stockDailyRecords);
        session.save(stockDailyRecords2);

		session.getTransaction().commit();
		System.out.println("Done");
	}
}
