package com.c2t.annotation.basic.ops;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class ListOperation2 {

	public static void main(String[] args) {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		Query q = session.createQuery("from Employee order by id desc");
		List <Employee> l = q.list();
		
	    for (int i=0;	 i<l.size();i++){
	    	Employee emp = l.get(i);
	    	
	    	System.out.println(emp.getCellphone());
	    	System.out.println(emp.getId());
	    	
	    	System.out.println("-----------------");
	    }
		
		
		
	}

}
