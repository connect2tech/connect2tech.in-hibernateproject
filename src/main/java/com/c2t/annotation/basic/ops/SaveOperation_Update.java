package com.c2t.annotation.basic.ops;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Restrictions;


public class SaveOperation_Update {

	public static void main(String[] args) {
		
		Employee empl = new Employee("Save", "Value", new Date(System.currentTimeMillis()), "911");
		
		SessionFactory sf = new AnnotationConfiguration()
        		.configure("com/c2t/annotation/basic/hibernate.cfg.xml")
                .buildSessionFactory();
		
		Session session = sf.openSession();

		session.beginTransaction();

		session.save(empl);
		
		empl.setFirstname("save and modified");
		
		session.getTransaction().commit();
		
		session.close();
		
		System.out.println("Done....");
	}
	
}
