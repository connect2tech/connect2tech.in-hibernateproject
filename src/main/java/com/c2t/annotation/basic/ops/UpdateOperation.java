package com.c2t.annotation.basic.ops;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class UpdateOperation {

	public static void main(String[] args) {


		SessionFactory sf = new AnnotationConfiguration().configure("com/c2t/annotation/basic/hibernate.cfg.xml")
				.buildSessionFactory();

		Session session = sf.openSession();
		
		
		//select * from employee_table where id=1
		Employee e = (Employee)session.get(Employee.class, 1L);
		
		System.out.println(e);
		
		e.setFirstname("Naresh Modified");
		
		
		session.beginTransaction();
		session.save(e);
		session.getTransaction().commit();
		
	}

}
