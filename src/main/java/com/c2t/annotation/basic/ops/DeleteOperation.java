package com.c2t.annotation.basic.ops;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class DeleteOperation {

	public static void main(String[] args) {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		Employee emp = (Employee) session.get(Employee.class, 1l);
		
		
		session.beginTransaction();

		session.delete(emp);
		
		session.getTransaction().commit();

		session.close();
		
		System.out.println("Delete done...");

	}

}
