package com.c2t.annotation.basic.ops;

import java.sql.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BatchProcessing {
	public static void main(String[] args) {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();

		for(int i=0;i<100;i++){
			Employee empl = new Employee("Jack" + i, "Bauer" + i, new Date(System.currentTimeMillis()), "911");
		
			session.save(empl);
			
			if(i%10 == 0){
				session.flush();
				session.clear();
			}
		
		}
		
		session.getTransaction().commit();
		session.close();

		System.out.println("done....");

	}
}
