package com.c2t.annotation.basic.ops;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;


public class MainApplication {

	public static void main(String[] args) {
		
		//listData();
		
		// Write to database...
		/*System.out.println("******* WRITE *******");
		Employee empl = new Employee("Naresh3", "Chaurasia", new Date(System.currentTimeMillis()), "911");
		empl = saveEmployee(empl);*/
		
		
		//Reading from database...
		/*empl = read(empl.getId());
		System.out.printf("%d %s %s \n", empl.getId(), empl.getFirstname(), empl.getLastname());*/
		//Read from database table.
		//list();
		
		
		//Employee e = read(2l);
		
		// Delete
		//System.out.println("******* DELETE *******");
		//deleteObj(e);
		/*Employee empl3 = read(empl.getId());
		System.out.println("Object:" + empl3);*/
		
		// Update
		/*System.out.println("******* UPDATE *******");
		Employee empl2 = read(104l); // read employee with id 1*/

		
		/*e.setFirstname("java");
		
		update(e);*/
		
		
		/*empl2 = read(1l); // read again employee with id 1
		System.out.println("Name Aftere Update:" + empl2.getFirstname());
		*/
		usingCriteria();
		
		//insertIntoBatch();
		
		/*List l  = listOrderBy();
		for(int i=0;i<l.size();i++){
			Employee temp = (Employee)l.get(i);
			System.out.println(temp.getFirstname()+" , "+temp.getLastname());
		}*/
		
		// Read
		/*System.out.println("******* READ *******");
		List employees = list();*/
		//System.out.println("Total Employees: " + employees.size());
		
		
		// Write
		/*System.out.println("******* WRITE *******");
		Employee empl = new Employee("Jack", "Bauer", new Date(System.currentTimeMillis()), "911");
		empl = save(empl);*/
		//empl = read(empl.getId());
		//System.out.printf("%d %s %s \n", empl.getId(), empl.getFirstname(), empl.getLastname());
		
	}
	
	private static void insertIntoBatch() {
		
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		
		for (int i = 0; i < 100; i++) {
			Employee empl = new Employee("Jack"+i, "Bauer"+i, new Date(System.currentTimeMillis()), "911");
			Long id = (Long) session.save(empl);
			empl.setId(id);
			
			if(i%10 == 0){
				session.flush();
				session.clear();
			}
		}
		
		session.getTransaction().commit();
		session.close();
		
		System.out.println("done....");

	}
	
	private static void usingCriteria(){
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		Criteria c = session.createCriteria(Employee.class);
		c.add(Restrictions.gt("id", 100l));

		
		List l = c.list();
		
		for(int i=0;i<l.size();i++){
			Employee e = (Employee)l.get(i);
			System.out.println(e.getFirstname()+ " , " + e.getLastname());
		}
		
		System.out.println("Done usingCriteria...");
		
	}
	
	

	private static List listData() {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		Query q = session.createQuery("from Employee where cellphone > 90");
		List list = q.list();
		
		for(int i=0;i<list.size();i++){
			Employee e = (Employee)list.get(i);
			
			System.out.println("e.getFirstname()="+e.getFirstname());
			System.out.println("e.getLastname()="+e.getLastname());
			
			System.out.println("---------------------------------------");
			
		}
		
		session.close();
		return list;
	}
	
	private static List listOrderBy() {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();

		List employees = session.createQuery("from Employee order by firstname").list();
		session.close();
		return employees;
	}
	
	private static List listGroupBy() {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();

		List employees = session.createQuery("from Employee group by lastname").list();
		session.close();
		return employees;
	}
	
	private static Employee read(Long id) {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		Employee emp = (Employee)session.get(Employee.class, id);
		
		System.out.println(emp.getFirstname());
		System.out.println(emp.getLastname());
		
		return emp;
	}
	
	
	private static Employee saveEmployee(Employee employee) {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();

		session.beginTransaction();

		Long id = (Long) session.save(employee);
		System.out.println("id of the object save in db::"+id);
		employee.setId(id);
		
		session.getTransaction().commit();
		
		session.close();

		return employee;
	}

	private static Employee update(Employee employee) {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();

		session.beginTransaction();

		session.update(employee);
		
		session.getTransaction().commit();
		
		session.close();
		return employee;

	}

	private static void deleteObj(Employee employee) {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		session.beginTransaction();
		
		session.delete(employee);
		
		session.getTransaction().commit();
		
		session.close();
	}
	
}
