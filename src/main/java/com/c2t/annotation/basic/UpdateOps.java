package com.c2t.annotation.basic;

import java.sql.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class UpdateOps {

	public static void main(String[] args) {

		SessionFactory sf = new AnnotationConfiguration().configure("com/c2t/annotation/basic/hibernate.cfg.xml")
				.buildSessionFactory();

		Session session = sf.openSession();

		Employee emp = (Employee)session.get(Employee.class, 1l);
		System.out.println(emp.getFirstname());

		emp.setFirstname("Naresh Modified");
		
		session.beginTransaction();
		session.save(emp);
		session.getTransaction().commit();
	}

}
