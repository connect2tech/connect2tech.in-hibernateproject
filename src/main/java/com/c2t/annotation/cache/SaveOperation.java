package com.c2t.annotation.cache;

import java.sql.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class SaveOperation {

	public static void main(String[] args) {

		Employee empl = new Employee("Naresh10", "Chaurasia10", new Date(System.currentTimeMillis()), "922");

		SessionFactory sf = new AnnotationConfiguration().configure("com/c2t/annotation/cache/hibernate.cfg.xml")
				.buildSessionFactory();

		Session session = sf.openSession();

		session.beginTransaction();

		session.save(empl);

		session.getTransaction().commit();
		session.close();

		System.out.println("Done....");
	}

}
