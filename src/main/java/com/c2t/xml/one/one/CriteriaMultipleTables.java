package com.c2t.xml.one.one;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

public class CriteriaMultipleTables {

	public static void main(String[] args) {

		SessionFactory sf = new Configuration().configure("com/c2t/xml/one/one/hibernate-many.cfg.xml")
				.buildSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Stock.class);
		criteria.setFetchMode("StockDetail", FetchMode.JOIN);

		List l = criteria.list();

		for (int i = 0; i < l.size(); i++) {
			Stock s = (Stock)l.get(i);
			StockDetail sd = s.getStockDetail();
			System.out.println(s.getStockName());
			System.out.println(sd.getCompDesc());
			System.out.println("--------------------");
		}

	}

}
